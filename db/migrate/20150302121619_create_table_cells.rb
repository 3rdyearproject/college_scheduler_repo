class CreateTableCells < ActiveRecord::Migration
  def change
    create_table :table_cells do |t|
      t.string :day
      t.string :start_time
      t.integer :room
      t.integer :subject_id

      t.timestamps null: false
    end
  end
end
