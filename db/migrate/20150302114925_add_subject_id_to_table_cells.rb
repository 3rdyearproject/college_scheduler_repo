class AddSubjectIdToTableCells < ActiveRecord::Migration
  def change
    add_column :table_cells, :subject_id, :integer
  end
end
