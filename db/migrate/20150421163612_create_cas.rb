class CreateCas < ActiveRecord::Migration
  def change
    create_table :cas do |t|
      t.date :date
      t.integer :time
      t.decimal :percentage
      t.integer :subject_id
      t.integer :course_id

      t.timestamps null: false
    end
  end
end
