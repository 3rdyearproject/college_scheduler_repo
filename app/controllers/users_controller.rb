class UsersController < ApplicationController

	def create
		params = user_params
		course = Course.find_by course_code: params[:course_name]
		id = course.id
		user = User.new(name: params[:name], email: params[:email], password: params[:password], password_confirmation: params[:password_confirmation], course_id: id)
		user.save
		render :nothing => true
	end

	def user_params
      params.permit(:name, :email, :password, :password_confirmation, :course_name)
    end
    def prof
    	render json: current_user
    end

    def friends
      @friends = User.where(course_id: current_user.course_id)
      render json: @friends
    end
   
end
