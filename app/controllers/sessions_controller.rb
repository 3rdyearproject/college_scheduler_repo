class SessionsController < ApplicationController

def create
    user = User.find_by email: (params[:email].downcase)
    
if user && user.authenticate(params[:password])
      log_in(user)
       render nothing: true
   
 else
    render nothing: true, status: :unauthorized
      
    end
 end

  # Remembers a user in a persistent session.


def destroy
    log_out

  end
end
