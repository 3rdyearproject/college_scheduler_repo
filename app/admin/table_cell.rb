ActiveAdmin.register TableCell do
permit_params :day, :start_time, :room, :subject_id, course_ids: []

form do |f|
        f.inputs "Details" do
          f.input :day
          f.input :start_time
          f.input :room
          f.input :courses
          f.input :subject # don't forget this one!
        end
        f.actions
      end
index do
  selectable_column
  column :id
  column :day
  column :start_time
  column :room
  column "Subject" do |table|
    Subject.find(table.subject_id).name
   end
  # column "Course Code" do |table|
   # Course.find(table.course_cells.course_id).course_code
   #end
  actions
end
end
