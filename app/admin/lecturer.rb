ActiveAdmin.register Lecturer do
permit_params :name, :email, :password, subject_ids:[], subjects_lecturers: [:subject_id, :lectuer_id], subjects: [:name, :id]

form do |f|
        f.inputs "Details" do
          f.input :name
          f.input :email
          f.input :subjects # don't forget this one!
        end
        f.actions
      end

 
 
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
