class User < ActiveRecord::Base
	  attr_accessor :remember_token

	belongs_to :course
	validates :email, presence: true,  uniqueness: true
	has_secure_password
	  validates :password, length: { minimum: 6 }
	   acts_as_messageable

	
def mailboxer_email(object)
  #Check if an email should be sent for that object
  #if true
  return email
  #if false
  #return nil
end
end
