class Subject < ActiveRecord::Base
	has_and_belongs_to_many :lecturers, :join_table => :lecturers_subjects
	has_many :cas
	has_many :table_cells
end
