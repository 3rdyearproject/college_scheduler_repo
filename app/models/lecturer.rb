class Lecturer < ActiveRecord::Base
	has_and_belongs_to_many :subjects, :join_table => :lecturers_subjects

end
