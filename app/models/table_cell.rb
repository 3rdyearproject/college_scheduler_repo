class TableCell < ActiveRecord::Base
	belongs_to :subject
	has_and_belongs_to_many :courses, :join_table => :course_cells
	#has_many :courses_cells
	#has_many :courses, :through => :courses_cell
	attr_accessor :sub_name
end
